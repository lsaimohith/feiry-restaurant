import { Injectable } from '@angular/core';
import { Users} from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
 users: Users[]=[];

 addUser(user) {

  this.users.push(user);
  console.log('Adding user in service' + user);

 }

 getUsers(){
   return this.users;
 }

  constructor() { }
}
