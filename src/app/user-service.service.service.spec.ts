import { TestBed, inject } from '@angular/core/testing';

import { UserService.ServiceService } from './user-service.service.service';

describe('UserService.ServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService.ServiceService]
    });
  });

  it('should be created', inject([UserService.ServiceService], (service: UserService.ServiceService) => {
    expect(service).toBeTruthy();
  }));
});
