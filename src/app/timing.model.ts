export class Timing{

  
    RestaurantId:string;
    startTime:number;
    endTime:number;
    isopen:boolean;

    constructor(id: string, start: number, end:number)
    {
        this.RestaurantId=id;
        this.startTime=start;
        this.endTime=end;
    }
    
}