import { Xliff } from "@angular/compiler";
import { OrderItem } from "../order/order.model";

export class Orders{
    name: string;
    address: string;
    number: string;
    email: string;
    emailConfirmation: string;
    optionalAddress: string;
    paymentOptions: string;
    orderItems: OrderItem[];
    id: number;
}

// export class Reorder {
//     id : number;
//     orderItems: [ {
//         quantity : number;
//         item : string;
//      }
//     ];
// }

// export class OrderItems {
//     quantity : number;
//     item : string;
// }