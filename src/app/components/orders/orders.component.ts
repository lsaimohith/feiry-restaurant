import { Component, OnInit } from '@angular/core';
import { OrdersService } from './orders.service';
import { Orders } from './orders.model';
import { Router } from '@angular/router';
import { LoginService } from '../security/login/login.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Users } from '../../user';
import { UserService } from '../../user-service.service.service';

@Component({
  selector: 'lacc-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  orders:Orders[];
  // view;
  disable:boolean=false;
  userForm: any;
  
  //VISHAL SINGH SAINI-- openform
    openForm() {
      document.getElementById("myForm").style.display = "block"; 
  }
  //VISHAL SINGH SAINI-- closeform
  closeForm() {
      document.getElementById("myForm").style.display = "none";
  }
  
  

  constructor(private ordersService:OrdersService, private router : Router, private service : OrdersService, private login : LoginService,
    private formBuild:FormBuilder, private userService: UserService ) { }

  ngOnInit() {
    this.orders = this.ordersService.Orders;
    this.userForm = this.formBuild.group({
      RestaurantReview : [' ' , Validators.required]
    });
  }
//VISHAL SINGH SAINI-- function to add review
addNewRevie(){
  const user: Users =  new Users();
  let formValue= this.userForm.value;
  user.RestaurantReview = formValue.RestaurantReview;
  console.log('Got request to add this user' + user);

  this.userService.addUser(user);
  console.log(this.userService.getUsers());
}


rev(event){
console.log(event);

if(event.length!=0){

  this.disable=true;
}
else{
  this.disable=false;
}

}

 //Vishal - Reorder from order details 
  reOrder(id) {
    for(let order of this.service.Orders) {
      if(order.id == id) {
        id = order.id;
        sessionStorage.setItem(this.login.user.userEmail, JSON.stringify(order));
      }
    }
    this.router.navigate(['restaurants', "1"]);
  }

  //Vishal - View details from Orders page
  viewDetails(id) {
    this.router.navigate(['viewDetails', id]);
  }

}
