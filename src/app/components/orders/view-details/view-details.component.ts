import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from '../../security/login/login.service';
import { OrdersService } from '../orders.service';

@Component({
  selector: 'lacc-view-details',
  templateUrl: './view-details.component.html',
  styleUrls: ['./view-details.component.css']
})

// Vishal - Displaying order details
export class ViewDetailsComponent implements OnInit {
orders;
username:string;
// items;
  constructor(private activeRoute : ActivatedRoute, private service : OrdersService, private login : LoginService) { 
      
    }

  ngOnInit() {
    this.username = this.login.user.userName;
    const orderId = this.activeRoute.snapshot.params["id"];
    this.orders = this.service.getOrdersById(orderId);
    console.log(this.orders);
  }

  display() {
    alert("Invoice Downloaded");
  }
}
