import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, startWith } from 'rxjs/operators';

@Component({
  selector: 'lacc-search-restaurant',
  templateUrl: './search-restaurant.component.html',
  styleUrls: ['./search-restaurant.component.css']
})
export class SearchRestaurantComponent implements OnInit {
  //Harish - created this component to search restaurants in the food page(restaurants component)
  searchField: FormControl;
  @Output() search = new EventEmitter<string>();
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.searchField = new FormControl();
    this.searchField.valueChanges.pipe(debounceTime(250),
      distinctUntilChanged(), startWith('')).subscribe(data => {
      this.search.emit(data);
      });
  }
}
