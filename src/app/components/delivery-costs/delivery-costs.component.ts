import {Component, Input, OnInit} from '@angular/core';
import { DeliveryCostsService } from './delivery-costs.service';

@Component({
    selector: 'lacc-delivery-costs',
    templateUrl: './delivery-costs.component.html',
    styleUrls: ['./delivery-costs.component.css']
})
export class DeliveryCostsComponent implements OnInit {

    @Input() delivery: number;
    @Input() itemsValue: number;
    //Harish - getting this value from the order component
    @Input() discount = 0;
    totalAmount: number;

   
    constructor(private deliveryService: DeliveryCostsService) {
    }

    ngOnInit() {
    }
 //Nikhil - It will make call to totalvalue function to calculate total amount and return to html file
 total() {
    if (this.itemsValue == 0) {
        return this.totalAmount = 0;
    }
    else {
        this.totalAmount = this.deliveryService.totalValue(this.delivery, this.itemsValue);
        return this.totalAmount - this.discount;
    }
  //  return this.delivery + this.itemsValue - this.discount;
    
}
//Nikhil - It will calculate the delivery cost. If item value is 0 then delivery amount will bre 0
deliveryCost() {
    if (this.itemsValue == 0) {
        return this.delivery = 0;
    }
    else {
        return this.delivery;
    }
}

//Nikhil - It will make call to calculateGST() method
calculateGST() {
    return this.deliveryService.calculateGST(this.delivery, this.itemsValue);;
}

//Nikhil - It will make call to calculateSGST() method
calculateSGST() {
    return this.deliveryService.calculateSGST(this.delivery, this.itemsValue);
}

//Nikhil - It will make call to calculateCGST() method
calculateCGST() {
    return this.deliveryService.calculateCGST(this.delivery, this.itemsValue);
}

    //Harish - subtracted the discount value
    // total(): number {
    //     return this.delivery + this.itemsValue - this.discount;
    // }

}
