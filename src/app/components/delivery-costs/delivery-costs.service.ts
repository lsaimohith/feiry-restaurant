import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeliveryCostsService {

  //Nikhil - This service file will calculate the GST, CGST, SGST and Total Amount
  totalAmount: number;
  gst: number;
  sgst: number;
  cgst: number;
  constructor() { }

  totalValue(itemsValue, delivery): number {
    this.totalAmount = delivery + itemsValue + this.gst;
    return this.totalAmount;
  }

  calculateGST(delivery, itemsValue) {
    this.gst = this.sgst + this.cgst;
    return this.gst;
  }

  calculateSGST(delivery, itemsValue) {
    this.sgst = (((delivery + itemsValue) * 2.5) / 100);
    return this.sgst;
  }

  calculateCGST(delivery, itemsValue) {
    this.cgst = (((delivery + itemsValue) * 2.5) / 100);
    return this.cgst;
  }
}