import { TestBed, inject } from '@angular/core/testing';

import { DeliveryCostsService } from './delivery-costs.service';

describe('DeliveryCostsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeliveryCostsService]
    });
  });

  it('should be created', inject([DeliveryCostsService], (service: DeliveryCostsService) => {
    expect(service).toBeTruthy();
  }));
});
