export interface User 
// {
//     name: string;
//     email: string;
//     accessToken: string;
    
// }

{
    _id: string,
    userName: string,
    userEmail: string,
    isRestaurant: boolean,
    userGender: string,
    userAge: number,
    contactNo: number, // Harish - added number to show in display profile
    userCity: string,
    userCountry: string,
    userProfileImageUrl: string
    // accessToken: string;
}
