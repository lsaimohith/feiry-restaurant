import {Component, Input, OnInit} from '@angular/core';
import {Restaurant} from './restaurant.model';

@Component({
    selector: 'lacc-restaurant',
    templateUrl: './restaurant.component.html'
})
export class RestaurantComponent implements OnInit {

    @Input() restaurant: Restaurant;

    constructor() {
    }

    ngOnInit() {
    }
    // VISHAL SINGH SAINI -- fetching the ratings with the functions

getBGcolorForRating(rating: number): string {
    return rating < 2.5 ? 'bg-red' : (rating < 3.5 ? 'bg-orange' : 'bg-green');
}

getStarForRating(rating: number, position: number): string {
    let className: string = '';
    switch (position) {
        case 1:
            className = rating >= 1 ? 'fa-star' : (rating < 1 && rating > 0 ? 'fa-star-half-o' : 'fa-star-o');
            break;
        case 2:
            className = rating >= 2 ? 'fa-star' : (rating < 2 && rating > 1 ? 'fa-star-half-o' : 'fa-star-o');
            break;
        case 3:
            className = rating >= 3 ? 'fa-star' : (rating < 3 && rating > 2 ? 'fa-star-half-o' : 'fa-star-o');
            break;
        case 4:
            className = rating >= 4 ? 'fa-star' : (rating < 4 && rating > 3 ? 'fa-star-half-o' : 'fa-star-o');
            break;
        case 5:
            className = rating == 5 ? 'fa-star' : (rating < 5 && rating > 4 ? 'fa-star-half-o' : 'fa-star-o');
            break;
        default:
            className = 'fa-star-o';
            break;
    }
    return className;
}


}
