import { analyzeAndValidateNgModules } from '@angular/compiler';
import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { LoginService } from '../../../security/login/login.service';
import { RestaurantsService } from '../../restaurants-json.service';
import {ShoppingCartService} from './shopping-cart.service';

@Component({
    selector: 'lacc-shopping-cart',
    templateUrl: './shopping-cart.component.html',
    styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

    @Input() id : number;
    userdata;
    previousUrl;
    userEmail;
    curId;

    constructor(private shooppingCartService: ShoppingCartService, 
        private login : LoginService, 
        private service : RestaurantsService,
        router: Router) {
            // this.curId = this.activeRoute.snapshot.params["id"];
            // console.log(this.curId);
            router.events
            .pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => {
            // console.log('prev:', event.url);
            this.previousUrl = event.url;
        });
    }

    ngOnInit() {

        // Vishal - Filling cart items for Reorder
        if(this.previousUrl == "/restaurants/1" ){
        if(this.userdata = JSON.parse(sessionStorage.getItem(this.login.user.userEmail))){
            for(let itm of this.userdata.orderItems){
                this.additems(itm);
            }
        }
        }
    }

    /**
     * Function that receives data from the menu item by propertBinding from the menu.component.html
     * @param item
     */

    addItem(item: any) {
        // console.log(item);
        return this.shooppingCartService.addItem(item);
    }

    additems(order) {
        const itm = order.menuId;
        // console.log(itm);
        const menus = this.service.menuItems;
        for(let menu of menus) {
            if(menu.id == itm) {
                this.addItem(menu);
            }
        }
    }

    clear() {
        this.shooppingCartService.clear();
    }

    items() {
        return this.shooppingCartService.items;
    }

    removeItem(item: any) {
        this.shooppingCartService.removeItem(item);
    }

    total() {
        return this.shooppingCartService.total();
    }


}
