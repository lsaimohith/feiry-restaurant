import {Observable} from 'rxjs/index';
import { Users } from '../../../user';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../user-service.service.service';
import { RestaurantsService } from '../restaurants-json.service';
import { User } from '../../security/login/login.model';
import { Review } from '../restaurant-detail/reviews/reviews.model';
import { LoginService } from '../../security/login/login.service';

@Component({
    selector: 'lacc-reviews',
    templateUrl: './reviews.component.html',
    styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {

    // reviews: Observable<Review>;
    reviews: Review[];
      users: Users[];
    imgReaction: string = '';
    today: number = Date.now();
     user: User;

    constructor(private restaurantsService: RestaurantsService,
                private route: ActivatedRoute,private service: UserService,
                private loginservice:LoginService) {
                    this.users=[];
                    this.users=this.service.getUsers();
                    
                }

    ngOnInit() {
        /**
         * In this context when obtaining the reviews you do not need to use subscribe because in the html component
         * when doing ngFor use the PIPE SYNC which already does the subscribe automatically
         * @type {Observable<Review>}
         */
        this.reviews = this.restaurantsService.getReviewsOfRestaurants(this.route.parent.snapshot.params['id']);
        this.user = this.loginservice.user;

        // this.restaurantsService.getReviewsOfRestaurants(this.route.parent.snapshot.parent.params['id'])
        //     .pipe(
        //         tap(reviews => console.log('review: ', reviews))
        //     )
        //     .subscribe(reviews => this.reviews = reviews);

    }

}



