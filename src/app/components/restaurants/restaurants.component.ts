import {Component, OnDestroy, OnInit} from '@angular/core';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import {switchMap, tap, debounceTime, distinctUntilChanged, catchError, takeWhile} from 'rxjs/operators';

import {Restaurant} from './restaurant/restaurant.model';
import {RestaurantsService} from './restaurants-json.service';

@Component({
    selector: 'lacc-restaurants',
    templateUrl: './restaurants.component.html',
    styleUrls: ['./restaurants.component.css']
})
export class RestaurantsComponent implements OnInit, OnDestroy {

    restaurants: Restaurant[] = [];

    ////Harish - addition fot the search restaurant backlog
    search$ = new Subject<string>();
    finalRestaurants = [];
    restaurants$ = new BehaviorSubject<any[]>(this.restaurantService.getAllRestaurants());
    isAlive = true;
    searchName: string = '';

    constructor(private restaurantService: RestaurantsService) {
    }

    ngOnInit() {
        // this.restaurantService.getAllRestaurants()
        //     .pipe(
        //         //tap(restaurants => console.log('R: ', restaurants))
        //     )
        //     .subscribe(restaurants => this.restaurants = restaurants);

        this.restaurants = this.restaurantService.getAllRestaurants();

        //Harish - addition for searching the restaurants in the food page
        //console.log("Restaurants$ : " + this.restaurants$);
        combineLatest([this.search$, this.restaurants$])
            .pipe(takeWhile(() => this.isAlive = true)).subscribe(data => {
                //console.log("data : " + data);
                const search = data[0];
                this.searchName = search;
                const restaurants = data[1];
            //   //  if (!search) {
            //         this.finalRestaurants = data[1];
                    
            //     } else {
            //         this.finalRestaurants = restaurants.filter((restaurant) => restaurant.name.toLowerCase()
            //             .indexOf(search.toLowerCase()) !== -1);
            //     }
            });
    }
    ngOnDestroy(): void {
        this.isAlive = false;
    }

}
