import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Coupons } from './coupons';

@Component({
  selector: 'lacc-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.css']
})
export class CouponComponent implements OnInit {

  
  @Output() coupon = new EventEmitter<number>();
  @Output() modalClose = new EventEmitter<void>();
  //Harish - All changes in this file
  @Input() itemsValue: number;
  @Input() showCoupons: boolean;
  coupons: Coupons[];
  message='';

  constructor() { 
    this.coupons = [];
    this.coupons.push(new Coupons("SAVE20", 200, 20));
    this.coupons.push(new Coupons("SAVE30", 300, 30));
    this.coupons.push(new Coupons("SAVE50", 500, 50));
  }

  applyCoupon(code: string) {
    code = code.toUpperCase();
    for(let x of this.coupons) {
      if(x.coupon === code && this.itemsValue>x.amount){
        this.message = 'Coupon code accepted and used';
        this.coupon.emit(x.discount);
        return;
      }
      else if(x.coupon === code && this.itemsValue<=x.amount){
        this.message = 'Coupon is applicable for the order above ' + x.amount + ' only.';
        return;
      }
    }
    this.message = 'Invalid coupon !!';
  }

  ngOnInit() {
  }

  onClose(){
      this.modalClose.emit();
  }
}
