//Harish - Created this file to define the coupon structure
export class Coupons {
    coupon: string;
    amount: number;
    discount: number;

    constructor(coupon: string, amount: number, discount: number) {
        this.coupon = coupon;
        this.amount = amount;
        this.discount = discount;
    }
}