import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import {OrderService} from './order.service';
import {CartItem} from '../restaurants/restaurant-detail/shopping-cart/cart-item.model';
import {PATTERS} from '../shared/patterns';
import {RadioOption} from '../shared/radio/radio-option.model';
import {Order, OrderItem, DeliveryAddress} from './order.model';
import {tap} from 'rxjs/operators';
import { LoginService } from '../security/login/login.service';
import { User } from '../security/login/login.model';

import { UserService } from '../../user-service.service.service';

@Component({
    selector: 'lacc-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
    userForm: FormGroup;
    orderForm: FormGroup;
    delivery: number = 10;
    orderId: string;
    isDeliverySummary:boolean = false;
    user : User;
    address = new DeliveryAddress();
    userdata;
    opac:boolean=false;
    rating:number;
    order_id:number;

    

    //Harish - variables used in coupon backlog
    discount: number = 0;
    percentDiscount: number = 0;

    paymentOptions: RadioOption[] = [
        {label: 'Money', value: 'MON'},
        {label: 'Credit cart', value: 'CC'},
        {label: 'Debit cart', value: 'CD'},
        {label: 'Other', value: 'NN'}
    ];

    constructor(private orderService: OrderService,
                private router: Router,
                private formBuilder: FormBuilder,
                private login : LoginService,
                private formBuild:FormBuilder,
                private service:UserService
                ) {
                    this.order_id = 0;
        
    }

    ngOnInit() {
        this.opac=false;
        this.userForm = this.formBuild.group({
            RestaurantReview : [' ']
          });
        //Vishal - fetching address details from localstorage to display
        this.userdata = JSON.parse(localStorage.getItem(this.login.user.userEmail));
        if(this.userdata) {
            // console.log(this.userdata);
            this.setForm();
        }

        else {
        this.orderForm = this.formBuilder.group({
            // name: new FormControl('', {
            //     validators: [Validators.required, Validators.minLength(3)]
            // }),
            address: new FormControl('', [Validators.required, Validators.minLength(5)]),
            number: new FormControl('', [Validators.required, Validators.pattern(PATTERS.number)]),
            landmark: new FormControl('',[Validators.required]),
            zipcode: new FormControl('', [Validators.required, Validators.pattern(PATTERS.number)]),
            addresstag: new FormControl(''),
            checkAddress : new FormControl(true),
            // email: new FormControl('', [Validators.required, Validators.pattern(PATTERS.email)]),
            // emailConfirmation: new FormControl('', [Validators.required, Validators.pattern(PATTERS.email)]),
            // optionalAddress: this.formBuilder.control(''),
            paymentOption: new FormControl('card',[Validators.required]),
        }
        // ,{validators: [this.emailEqualsTo], updateOn: 'blur'}
        );
    }
    }
    //addition
    // stars:number;
    // user: User;

    openForm() {
        this.opac=true;
        this.order_id=this.order_id+1;
        document.getElementById("myForm").style.display = "block";
        // document.getElementById("myForm").style.border = "none";

    }
   // vishal singh saini -- storing order_id and rating 
   change(event){
    this.rating=event.target.value;
    let y =JSON.stringify(this.rating);
    localStorage.setItem(this.order_id.toString(),y);
    console.log(y);
    console.log(this.rating);
}

    setForm() {
        // console.log("set");
        this.orderForm = this.formBuilder.group({
        address: new FormControl(this.userdata.address, [Validators.required, Validators.minLength(5)]),
        number: new FormControl(this.userdata.number, [Validators.required, Validators.pattern(PATTERS.number)]),
        landmark: new FormControl(this.userdata.landmark,[Validators.required]),
        zipcode: new FormControl(this.userdata.zipCode, [Validators.required, Validators.pattern(PATTERS.number)]),
        addresstag: new FormControl(''),
        checkAddress : new FormControl(true),
        paymentOption: new FormControl('card',[Validators.required]),
        });
      }

    // private emailEqualsTo(group: AbstractControl): { [key: string]: boolean } {
    //     const email = group.get('email');
    //     const emailConfirmation = group.get('emailConfirmation');

    //     if (!email || !emailConfirmation) {
    //         return undefined;
    //     }

    //     if (email.value !== emailConfirmation.value) {
    //         return {emailsNotMatch: true};
    //     }

    //     return undefined;
    // }

    itemsValue(): number {
        return this.orderService.itemsValue();
    }

    cartItems(): CartItem[] {
        return this.orderService.cartItems();
    }

    decreaseQtd(item: CartItem) {
        return this.orderService.decreaseQtd(item);
    }

    increaseQtd(item: CartItem) {
        return this.orderService.increaseQtd(item);
    }

    remove(item: CartItem) {
        return this.orderService.remove(item);
    }

    checkOrder(order: Order) {
        order.orderItems = this.cartItems()
            .map((item: CartItem) => new OrderItem(item.quantity, item.menuItem.id));

        // console.log(order);

        this.orderService.checkOrder(order)
            .pipe(
                tap((orderId: string) => {
                    this.orderId = orderId;
                    console.log(order);
                })
            )
            .subscribe(() => {
                this.router.navigate(['/order-summary']);
                // this.orderService.clear();
            });
    }

    isOrderCompleted(): boolean {
        return this.orderId !== undefined;
    }

    // Vishal - Store the delivery address details for future orders
    showDeliverySummary(){
        this.isDeliverySummary = !this.isDeliverySummary;
        if(this.orderForm.value.checkAddress) {
            this.address.usrmail = this.login.user.userEmail; 
            this.address.address = this.orderForm.value.address;
            this.address.landmark = this.orderForm.value.landmark;
            this.address.number = this.orderForm.value.number;
            this.address.zipCode = this.orderForm.value.zipcode;
            localStorage.setItem(this.address.usrmail, JSON.stringify(this.address));
        }
    }

    //Harish - for discount calculation after applying the coupon
    discountValue() {
        if(this.orderService.itemsValue() <= (this.percentDiscount*10)){
            this.discount = 0;
        }
        else{
            this.discount =  this.orderService.itemsValue();
            this.discount = this.discount * (this.percentDiscount/100);
        }
        return this.discount;
    }

    //Harish - for discount calculation when the coupon coupon is applied for first time 
    calculatedDiscount(percntDiscount : number) {
        this.percentDiscount = percntDiscount;
        this.discount =  this.orderService.itemsValue();
        this.discount = this.discount * (percntDiscount/100);
    }
}
