class OrderItem {
    constructor(public quantity: number, public menuId: string) {
    }
}

class Order {
    constructor(public address: string,
                public number: number,
                public optionalAddress: string,
                public paymentOptions: string,
                public orderItems: OrderItem[] = [],
                public id?: string) {
    }
}

class DeliveryAddress {
    public usrmail : any;
    public address : string;
    public number : number;
    public landmark : string;
    public zipCode : number;
}
export {Order, OrderItem, DeliveryAddress};
