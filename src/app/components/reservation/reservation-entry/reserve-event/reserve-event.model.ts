export interface userDetails {
    user_id: string,
    restaurant_id: string,
    guestCount: string,
    bookingDate: Date,
    bookingTime: string,
    guestName: string,
    emailID: string,
    phoneNumber: string,
    instructions: string
}


