import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantsService } from '../../../restaurants/restaurants-json.service';
import { MenuItem } from '../../../restaurants/restaurant-detail/menu-item/menu-item.model';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LoginService } from '../../../security/login/login.service';

@Component({
  selector: 'lacc-reserve-event',
  templateUrl: './reserve-event.component.html',
  styleUrls: ['./reserve-event.component.css']
})
export class ReserveEventComponent implements OnInit {
  menu: MenuItem[];
  id: string;
  //Prevents user from selecting past dates
  // theDate: any;
  // date1 = new Date();
  // today = this.date1.getDate();
  // month = this.date1.getMonth();
  // year = this.date1.getFullYear();
  // myFilter = (d: Date): boolean => {
  //   const day = d.getDate();
  //   const month = d.getMonth();
  //   const year = d.getFullYear();
  //   return day >= this.today || month > this.month || year > this.year;
  // }

  constructor(private route: ActivatedRoute, private restaurantService: RestaurantsService, private router: Router, private loginService: LoginService) {
  }

  ngOnInit() {
    this.id = this.route.parent.snapshot.params['id'];
    this.menu = this.restaurantService.getMenuOfRestaurant('');
  }

  navigateToReservationStatusPage(bookingForm: NgForm) {
    let EventBookingDetailsVariable = bookingForm.value;
    let user;
    if (this.loginService.isLoggedIn() === true) {
      user = this.loginService.returnUser();
      localStorage.setItem(user,JSON.stringify(EventBookingDetailsVariable));
  }
  else{
      this.loginService.handleLogin();
  }
  this.router.navigate(['/reservation/reservation-status']);
  }
}

  
