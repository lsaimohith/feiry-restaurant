import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import { LoginService } from '../../../security/login/login.service';
import { NgForm } from '@angular/forms';


@Component({
    selector: 'lacc-reserve-table',
    templateUrl: './reserve-table.component.html',
    styleUrls: ['./reserve-table.component.css']
})

export class ReserveTableComponent implements OnInit {
//backlog to get user-details for reserving a table and printing their details on a new page. 
    //Backlog cleared by: Eshwer 
    ngForm;
    errorNone = true;
    errorValue:string;
    date1 = new Date;
    day = this.date1.getDate();
    month = this.date1.getMonth();
    year = this.date1.getFullYear();
    isNameEmpty = false;
    isEmailInvalid = false;
    isInvalidDate = true;
    bookingDate: Date;

    constructor(private router: Router, private route: ActivatedRoute, private loginService: LoginService) {
        this.bookingDate = new Date();
    }

    ngOnInit() {
        
    }
//Storing in local storage 
navigateToReservationStatusPage(bookingForm: NgForm) {
    let TableBookingDetailsVariable = bookingForm.value;
    let user;
    if (this.loginService.isLoggedIn() === true) {
        user = this.loginService.returnUserID();
        localStorage.setItem(user,JSON.stringify(TableBookingDetailsVariable));
    }
    else{
        this.loginService.handleLogin();
    }
    this.router.navigate(['/reservation/reservation-status']);
}

//Form Validations
validateName(event){
    console.log(event.target.value);
    if(event.target.value.length!=0){
        this.isNameEmpty=false;
        this.errorValue = "";
    }
    else{
        console.log(event.target.value);
        this.isNameEmpty=true;
        this.errorValue = "*Name is required"
    }
}

//Backlog: User Cannot select date which has passed
//Backlog Cleard by: Eshwer 
//Alternative logic using Angular Material available in reserve-event component
validateDate(event){
    const todayDate = new Date();  
    let thisDay = todayDate.getDate();
    let thisMonth = todayDate.getMonth();
    let thisYear = todayDate.getFullYear();
    let userSelectedDate:Date = new Date(event.target.value);
    let userSelectedDay = userSelectedDate.getDate();
    let userSelectedMonth = userSelectedDate.getMonth();
    let userSelectedYear = userSelectedDate.getFullYear();
    if(userSelectedDay >= thisDay || userSelectedMonth > thisMonth || userSelectedYear >thisYear){
        this.isInvalidDate = false;
        this.errorValue = "";
    }
    else{
        this.isInvalidDate = true;
        this.errorValue = "*Enter Valid Date"
    }
}
}


