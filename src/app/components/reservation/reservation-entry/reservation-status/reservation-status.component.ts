import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantsService } from '../../../restaurants/restaurants-json.service';
import { LoginService } from '../../../security/login/login.service';

@Component({
  selector: 'lacc-reservation-status',
  templateUrl: './reservation-status.component.html',
  styleUrls: ['./reservation-status.component.css']
})
export class ReservationStatusComponent implements OnInit {
  userBookingDetails;
  instruction: string;
  statusReservation: string = "Sucesss";

  constructor(private route: ActivatedRoute, private restaurantService: RestaurantsService, private loginService: LoginService) { }

  ngOnInit() {
    this.getUsersReservationDetails();
  }

  getUsersReservationDetails() {
    let user;
    let userSpecificInstructions;
    if (this.loginService.isLoggedIn() === true) {
      try {
        this.statusReservation = "Confirmed";
        user = this.loginService.returnUserID();
        console.log(user);
        this.userBookingDetails = JSON.parse(localStorage.getItem(user));
        console.log(this.userBookingDetails);
      } 
      catch (e) {
        this.statusReservation = "Server Error";
        console.log(e);
      }
      finally {
        userSpecificInstructions = this.userBookingDetails.instructions;
        if(userSpecificInstructions.length === 0) {
          this.instruction = "None";
        }
      }
    }
  }
}

