import { Component, OnDestroy, OnInit } from '@angular/core';
import { Restaurant } from './../restaurants/restaurant/restaurant.model';
import { RestaurantsService } from './../restaurants/restaurants-json.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { Timing } from '../../timing.model';
import { LoginService } from '../security/login/login.service';


@Component({
    selector: 'lacc-reservation',
    templateUrl: './reservation.component.html',
    styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit, OnDestroy {
 // VISHAL SINGH SAINI -- VARIABLES DECLARATION FOR OPEN AND CLOSE STATUS
 lowtohigh: string;
 hightolow: string;
 open:number=1000;
 close:number=2400;
 currentTime:any;
 isOpen:boolean;
 timings: Timing[];
  //Nikhil - Declare variable to differentiate sort
 // lowtohigh: string;
 // hightolow: string;
  nameaz: string;
  nameza: string;
  favorite: string;
  login:boolean = false;
 

    restaurants: Restaurant[] = [];
    filters: any = {};
    selectedFilters: {} = {
        category: '',
        location: '',
        cuisine: '',
        sort:''
    };

    ////Harish - addition for the search restaurant backlog
    search$ = new Subject<string>();
    finalRestaurants:string[] = [];
    restaurants$ = new BehaviorSubject<any[]>(this.restaurantService.getAllRestaurants());
    isAlive = true;
    searchName: string = '';

    constructor(private restaurantService: RestaurantsService, 
        private route: ActivatedRoute,
        private router: Router,
        private loginService: LoginService) {
            this.timings=[];
    }

    ngOnInit() {
        this.restaurants = this.restaurantService.getAllRestaurants();
        this.restaurants = this.restaurants.sort((a, b) => b.rating - a.rating);
        this.filters = this.getFilterData();
        if (this.loginService.isLoggedIn() === true) {
            this.login = true;
        }
          let start = 1000;
            let end = 2200;
            let date = new Date();
            this.currentTime = date.getHours() + ''+ (date.getMinutes()<10?'0':'') + date.getMinutes();
            //addition
            for(let x in this.restaurants){
                this.timings.push(new Timing(this.restaurants[x].id, start, end));
                start += 1500;
                end += 1500;
                if(start>2400){
                    start = start - 2400;
                }
                if(end>2400){
                    end = end - 2400;
                }
                if(this.currentTime>=this.timings[x].startTime && this.currentTime<this.timings[x].endTime)
                { 
                    this.timings[x].isopen=true;
                }
                else
                {
                     this.timings[x].isopen=false;
                }
                this.filters = this.getFilterData();
            
            }    
    }
  //Nikhil - It will handle the restaurants being added to favorite
  handleAddToFavorites(id) {
    this.restaurantService.addUsersFavoriteRestaurant(id);
}

//Nikhil - It will handle the restaurants being removed to favorite
handleRemoveFromFavorites(id) {
    this.restaurantService.removeUsersFavoriteRestaurant(id);
}
 //Nikhil - It will filter the restaurants to sort by rating from high to low
    selectFilterRatingHighToLow(id: string, $event) {
        this.restaurants = this.restaurantService.getAllRestaurants();
        let currentElement: any = $event.target || $event.srcElement;
        if (document.getElementById(id).getElementsByClassName('active')[0] !== undefined) {
            document.getElementById(id).getElementsByClassName('active')[0].classList.remove('active');
        }
        this.hightolow = "hightolow";
        currentElement.classList.add('active');
        this.filterRestaurants(id, currentElement.innerText);
    }

    //Nikhil - It will filter the restaurants to sort by rating from low to high
    selectFilterRatingLowToHigh(id: string, $event) {
        this.restaurants = this.restaurantService.getAllRestaurants();
        let currentElement: any = $event.target || $event.srcElement;
        if (document.getElementById(id).getElementsByClassName('active')[0] !== undefined) {
            document.getElementById(id).getElementsByClassName('active')[0].classList.remove('active');
        }
        this.lowtohigh = "lowtohigh";
        currentElement.classList.add('active');
        this.filterRestaurants(id, currentElement.innerText);
    }

    //Nikhil - It will filter the restaurants to sort by name A to Z
    selectFilterNameAZ(id: string, $event) {
        this.restaurants = this.restaurantService.getAllRestaurants();
        let currentElement: any = $event.target || $event.srcElement;
        if (document.getElementById(id).getElementsByClassName('active')[0] !== undefined) {
            document.getElementById(id).getElementsByClassName('active')[0].classList.remove('active');
        }
        this.nameaz = "nameaz";
        currentElement.classList.add('active');
        this.filterRestaurants(id, currentElement.innerText);
    }

    //Nikhil - It will filter the restaurants to sort by name Z to A
    selectFilterNameZA(id: string, $event) {
        this.restaurants = this.restaurantService.getAllRestaurants();
        let currentElement: any = $event.target || $event.srcElement;
        if (document.getElementById(id).getElementsByClassName('active')[0] !== undefined) {
            document.getElementById(id).getElementsByClassName('active')[0].classList.remove('active');
        }
        this.nameza = "nameza";
        currentElement.classList.add('active');
        this.filterRestaurants(id, currentElement.innerText);
    }

    //Nikhil - It will filter the restaurants to sort by favorite
    selectFilterFavorite(id: string, $event) {
        let currentElement: any = $event.target || $event.srcElement;
        if (document.getElementById(id).getElementsByClassName('active')[0] !== undefined) {
            document.getElementById(id).getElementsByClassName('active')[0].classList.remove('active');
        }
        this.favorite = "favorite";
        currentElement.classList.add('active');
        this.filterRestaurants(id, currentElement.innerText);
    }


    //Harish 
    ngOnDestroy(): void {
        this.isAlive = false;
    }

    getBGcolorForRating(rating: number): string {
        return rating < 2.5 ? 'bg-red' : (rating < 3.5 ? 'bg-orange' : 'bg-green');
    }

    getStarForRating(rating: number, position: number): string {
        let className: string = '';
        switch (position) {
            case 1:
                className = rating >= 1 ? 'fa-star' : (rating < 1 && rating > 0 ? 'fa-star-half-o' : 'fa-star-o');
                break;
            case 2:
                className = rating >= 2 ? 'fa-star' : (rating < 2 && rating > 1 ? 'fa-star-half-o' : 'fa-star-o');
                break;
            case 3:
                className = rating >= 3 ? 'fa-star' : (rating < 3 && rating > 2 ? 'fa-star-half-o' : 'fa-star-o');
                break;
            case 4:
                className = rating >= 4 ? 'fa-star' : (rating < 4 && rating > 3 ? 'fa-star-half-o' : 'fa-star-o');
                break;
            case 5:
                className = rating == 5 ? 'fa-star' : (rating < 5 && rating > 4 ? 'fa-star-half-o' : 'fa-star-o');
                break;
            default:
                className = 'fa-star-o';
                break;
        }
        return className;
    }

    clearFilter() {
        let activeElements: any = document.getElementsByClassName('filter-option');
        for (let i: number = 0; i < activeElements.length; i++) {
            activeElements[i].classList.remove('active');
        }
        Object.keys(this.selectedFilters).forEach(k => {
            this.selectedFilters[k] = '';
        });
        this.restaurants = this.restaurantService.getAllRestaurants();
    }

    selectFilter(id: string, $event) {
        let currentElement: any = $event.target || $event.srcElement;
        if (document.getElementById(id).getElementsByClassName('active')[0] !== undefined) {
            document.getElementById(id).getElementsByClassName('active')[0].classList.remove('active');
        }
        currentElement.classList.add('active');
        this.filterRestaurants(id, currentElement.innerText);
    }

    navigateToReservationPage(redirectTo: string, id: string) {
        this.router.navigate(['reservation/reservation-entry', id, redirectTo]);
    }

    getFilterData(): {} {
        this.filters = {
            'category': this.getFiterObject('category'),
            'location': this.getFiterObject('area'),
            'cuisine': this.getFiterObject('type')
        };

        return this.filters;
    }
 //changes start here
 navigateToBookEventPage(redirectTo: string, id: string) {
    this.router.navigate(['reservation/reservation-entry', id, redirectTo]);
}
//changes end here

    getFiterObject(type: string): any[] {
        return this.restaurants.map(item => item[type])
            .filter((value, index, self) => self.indexOf(value) === index);
    }

    getFilterCount(name:string, value:string):number{
        return this.restaurants.filter(x => x[name] === value).length;
    }

    filterRestaurants(type, name) {
        this.selectedFilters[type] = name;
        this.restaurants = this.restaurantService.getAllRestaurants();
        if (this.selectedFilters['category'] !== '') {
            this.restaurants=this.restaurants.filter(x=>x.category === this.selectedFilters['category']);
        }
        if (this.selectedFilters['location'] !== '') {
            this.restaurants=this.restaurants.filter(x=>x.area === this.selectedFilters['location']);
        }
        if (this.selectedFilters['cuisine'] !== '') {
            this.restaurants=this.restaurants.filter(x=>x.type === this.selectedFilters['cuisine']);
        }
        if (this.selectedFilters['sort'] !== '') {
            this.restaurants = this.restaurants.sort((a,b)=>a[this.selectedFilters['sort'].toLowerCase()]-b[this.selectedFilters['sort'].toLowerCase()]);
        }
        if (this.selectedFilters['category'] !== '') {
            console.log("Inside Category");

            this.restaurants = this.restaurants.filter(x => x.category === this.selectedFilters['category']);
        }

        if (this.selectedFilters['location'] !== '') {
            this.restaurants = this.restaurants.filter(x => x.area === this.selectedFilters['location']);
        }

        if (this.selectedFilters['cuisine'] !== '') {
            this.restaurants = this.restaurants.filter(x => x.type === this.selectedFilters['cuisine']);
        }
        if (this.selectedFilters['sort'] !== '' && this.favorite === "favorite") {
            //Nikhil - Sort By Favorite
            if (this.loginService.isLoggedIn() === true) {
                this.restaurants = this.restaurantService.getUsersFavoriteRestaurant();
            }
            else {
                this.loginService.handleLogin();
            }
            this.favorite = null;
        }
        
        if (this.selectedFilters['sort'] !== '' && this.nameaz === "nameaz") {
            //Nikhil - Sort By Name A-Z
            this.restaurants = this.restaurants.sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));
            this.nameaz = null;
        }

        if (this.selectedFilters['sort'] !== '' && this.nameza === "nameza") {
            //Nikhil - Sort By Name Z-A
            this.restaurants = this.restaurants.sort((a, b) => b.name.toLowerCase().localeCompare(a.name.toLowerCase()));
            this.nameza = null;
        }

        if (this.selectedFilters['sort'] !== '' && this.hightolow === "hightolow") {
            //Nikhil - Sort By Rating High To Low
            this.restaurants = this.restaurants.sort((a, b) => b[this.selectedFilters['sort'].toLowerCase()] - a[this.selectedFilters['sort'].toLowerCase()]);
            this.hightolow = null;
        }

        if (this.selectedFilters['sort'] !== '' && this.lowtohigh === "lowtohigh") {
            //Nikhil - Sort By Rating Low To High
            this.restaurants = this.restaurants.sort((a, b) => a[this.selectedFilters['sort'].toLowerCase()] - b[this.selectedFilters['sort'].toLowerCase()]);
            this.lowtohigh = null;
        }

    }

}
