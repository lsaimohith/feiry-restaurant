import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { userInfo } from 'os';
import { User } from '../security/login/login.model';
import { LoginService } from '../security/login/login.service';

@Component({
  selector: 'lacc-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  //Harish - created this component to show profile 
  currUser: User;
  
  constructor(private loginService: LoginService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.currUser = this.loginService.user;
  }

}
